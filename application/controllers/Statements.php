<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statements extends CI_Controller {

	public function censorship() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/censorship'; //This corresponds to /application/views/statements/censorship.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
  }

	public function letter_to_arvind_kejriwal() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/letter-to-arvind-kejriwal'; //This corresponds to /application/views/statements/letter-to-arvind-kejriwal.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}

	public function blackmoney() {
		$data['title'] = 'Official Statements';
    $data['content'] = 'statements/blackmoney'; //This corresponds to /application/views/statements/letter-to-arvind-kejriwal.php
    $this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Elections extends CI_Controller {
	public function kerala2016() {
		$data['title'] = "Kerala Assembly Election 2016";
		$data['content'] = "elections/kerala2016"; //This corresponds to /application/views/elections/kerala2016.php
		$this->load->view('templates/default',$data); //This loads the view /application/views/templates/default.php
	}
}

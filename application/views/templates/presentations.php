<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <title><?php echo isset($title) ? $title." | Indian Pirates" : "Indian Pirates" ;?></title>
	<meta name="description" content="<?php echo isset($description) ? $description." | Indian Pirates" : "The Official Website of Indian Pirates" ;?>">
	<meta name="author" content="<?php echo isset($author) ? $author." | Indian Pirates" : "Indian Pirates Web Team" ;?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/presentations/reveal.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/presentations/theme.css" id="theme">

	<!-- If the query includes 'print-pdf', use the PDF print sheet -->
	<script>
		document.write( '<link rel="stylesheet" href="css/print/' + ( window.location.search.match( /print-pdf/gi ) ? 'pdf' : 'paper' ) + '.css" type="text/css" media="print">' );
	</script>

	<!--[if lt IE 9]>
	<script src="<?php echo base_url(); ?>assets/js/presentations/html5shiv.js"></script>
	<![endif]-->
</head>
<body>
  <?php $this->load->view($presentation_content); ?>
  <script src="<?php echo base_url(); ?>assets/js/presentations/head.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/presentations/reveal.js"></script>
  <script>
    // Full list of configuration options available here:
    // https://github.com/hakimel/reveal.js#configuration
    Reveal.initialize({
			slideNumber: 'c/t',
			transition: 'convex'
		});
  </script>
</body>
</html>
